/****************************************************************************
** Copyright (c) 2024 Adam Higerd
** See the Qxt AUTHORS file for a list of authors and copyright holders.
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the LibQxt project nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** <http://bitbucket.org/libqxt/libqxt>  <chighland@gmail.com>
*****************************************************************************/

/*!
\class QxtWebFolderService

\inmodule QxtWeb

\brief The QxtWebFolderService class exposes the contents of a folder in the
filesystem via HTTP.

QxtWebFolderService looks up the requested URL as a relative path within a
specified folder. If the path is contained within the folder, and the file
exists and is readable, then the content of the file is served as the response
to the request.

If the URL path is empty, \a defaultPath is used to select which file to serve.
The most common use of this is to serve a \c index.html file.

If the file is not found, the pageNotFound(QxtWebRequestEvent* event) function
is used to handle the request.
*/

#include "qxtwebfolderservice.h"
#include "qxtwebevent.h"
#include <QDir>
#include <QFile>
#include <QMimeDatabase>

#ifndef QXT_DOXYGEN_RUN
class QxtWebFolderServicePrivate : public QxtPrivate<QxtWebFolderService>
{
public:
  QxtWebFolderServicePrivate() : defaultPath("index.html") {}

  QString root;
  QString defaultPath;
};
#endif

/*!
 * Creates a QxtWebFolderService that serves files from the specified \a root folder.
 */
QxtWebFolderService::QxtWebFolderService(const QString& root, QxtAbstractWebSessionManager* sm, QObject* parent)
: QxtAbstractWebService(sm, parent)
{
  QXT_INIT_PRIVATE(QxtWebFolderService);
  qxt_d().root = root;
}

/*!
 * Returns the default path.
 *
 * If the requested URL is an empty path, then this is used to choose which
 * which file will be served.
 *
 * The default value is \c index.html.
 *
 * \sa setDefaultPath
 */
QString QxtWebFolderService::defaultPath() const
{
  return qxt_d().defaultPath;
}

/*!
 * Sets the default path.
 *
 * \sa defaultPath
 */
void QxtWebFolderService::setDefaultPath(const QString& path)
{
  qxt_d().defaultPath = path;
}

/*!
 * \reimp
 */
void QxtWebFolderService::pageRequestedEvent(QxtWebRequestEvent* event)
{
  QDir dir(qxt_d().root);
  QString path = event->url.path();
  while (path.startsWith("/")) {
    path.remove(0, 1);
  }
  if (path.isEmpty()) {
    path = qxt_d().defaultPath;
  }
  path = dir.absoluteFilePath(path);
  if (dir.relativeFilePath(path).startsWith("../") || !QFile::exists(path)) {
    unhandled(event);
    return;
  }

  QFile* file = new QFile(path, this);
  if (!file->open(QIODevice::ReadOnly)) {
    delete file;
    unhandled(event);
    return;
  }

  QxtWebPageEvent* response = new QxtWebPageEvent(event->sessionID, event->requestID, file);
  response->streaming = false;
  response->contentType = QMimeDatabase().mimeTypeForFile(path).name().toUtf8();
  postEvent(response);
}
